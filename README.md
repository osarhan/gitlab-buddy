# Lab Partner - GitLab Merge Request Assistant

![Gitlab Pipeline Status](https://img.shields.io/gitlab/pipeline-status/lab-partner%2Flab-partner?logo=rocket&logoColor=green&label=Pipeline%20Status)
![](https://img.shields.io/chrome-web-store/v/lkepfjciookjhedanolcnnclgokpkedc?logo=googlechrome)
![](https://img.shields.io/chrome-web-store/users/lkepfjciookjhedanolcnnclgokpkedc?label=chrome%20users&logo=googlechrome&color=blue)
![](https://img.shields.io/amo/v/lab-partner?label=firefox&logo=firefoxbrowser)
![](https://img.shields.io/amo/users/lab-partner?label=firefox%20users&logo=firefox&color=blue)

**Never Miss a Merge Request Again!**

Lab Partner is the ultimate Chrome extension for managing GitLab merge requests (MRs) effortlessly. Designed for developers, team leads, and managers, this extension provides a centralized dashboard to track, review, and act on MRs without the hassle of manual updates.

---

## Why Choose Lab Partner?

### Seamless Integration
- **No Personal Access Token Required**: Lab Partner uses your existing GitLab session, so you’re up and running in seconds.

### Centralized Dashboard
- **All MRs in One Place**: View MRs assigned to you, reviewed by you, or created by you—across multiple repositories and groups.
- **Subscribe to Projects or Groups**: Stay informed about MRs in specific projects or groups, even if they aren’t assigned to you or don’t follow group approval rules.

### Real-Time Insights
- **Quick Snapshot**: See MR statuses (pending, approved, merged) with direct links to the request, repository, author, and CI/CD pipelines.

### Smart Filters
- **Focus on What Matters**: Customize your dashboard to show only assigned MRs, group approvals, or those with unresolved conflicts.

### Stay Informed
- **Track Mentions and Comments**: Stay updated on conversations and decisions with real-time notifications.
- **Never Miss Important MRs**: Subscribe to projects or groups to ensure you’re always aware of MRs that matter to you, even if they aren’t assigned or reviewed.

### Conflict Alerts
- **Identify Conflicts Quickly**: Prioritize troubleshooting and keep workflows smooth by spotting merge conflicts early.

### Declutter Your Workflow
- **Hide Irrelevant MRs**: Keep your dashboard clean and focused on what’s important.

---

## Key Features

- **Effortless Setup**: No tokens, no hassle—just log in and go.
- **Comprehensive MR Tracking**: Assignee, reviewer, group approvals, and created MRs all in one view.
- **Project and Group Subscriptions**: Subscribe to projects or groups to track MRs that are important to you, even if they aren’t assigned or reviewed.
- **Actionable Notifications**: Direct links to MRs, pipelines, and conversations for quick actions.
- **Customizable Filters**: Tailor your dashboard to show only the MRs that matter to you.
- **Enhanced Productivity**: Save time and streamline your GitLab workflow.

---

## Who Is It For?

- **Developers**: Stay on top of assigned MRs and reviews.
- **Team Leads**: Track group approvals and manage team contributions.
- **Managers**: Focus on high-priority MRs with filtered views.
- **Anyone Who Cares About MRs**: Subscribe to projects or groups to ensure you never miss important updates, even if MRs aren’t assigned or reviewed.

---

## Installation

1. **Download from the Chrome Web Store**:
   - Visit the [Lab Partner Chrome Web Store page](https://chromewebstore.google.com/detail/lab-partner/lkepfjciookjhedanolcnnclgokpkedc) and click **Add to Chrome**.

2. **Log In to GitLab**:
   - Open the extension and log in to your GitLab account. No personal access token is required!

3. **Start Using Lab Partner**:
   - Your MR dashboard will automatically populate with all relevant merge requests.

---

## How to Use

1. **Open the Extension**:
   - Click the Lab Partner icon in your Chrome toolbar to open the dashboard.

2. **View Your MRs**:
   - See all MRs assigned to you, reviewed by you, or created by you.

3. **Subscribe to Projects or Groups**:
   - Add projects or groups to your subscription list to track MRs that are important to you, even if they aren’t assigned or reviewed.

4. **Apply Filters**:
   - Use the filter options to focus on specific MRs (e.g., unresolved conflicts, group approvals).

5. **Take Action**:
   - Click on any MR to open it in GitLab, view pipelines, or check comments.

---

## Feedback and Support

We’d love to hear from you! If you have any questions, suggestions, or issues, please:

- **Submit Feedback**: [Open an issue on Gitlab](https://gitlab.com/lab-partner/lab-partner/-/issues).
- **Contact Us**: Email us at [omarsarhan@gmail.com](mailto:omarsarhan@gmail.com).

---

## Boost Your GitLab Productivity Today!

Lab Partner is the must-have tool for anyone using GitLab to manage code reviews and merge requests. Download now and take control of your MR workflow!