import React, { useState, useCallback } from 'react'
import {
  addToast,
  Autocomplete,
  AutocompleteItem,
  AutocompleteSection,
  Avatar,
  Button,
  ButtonGroup,
  Listbox,
  ListboxItem,
  ListboxSection,
  Spacer
} from '@heroui/react'
import { sendToBackground } from '@plasmohq/messaging'
import { ACTIONS } from '~lib/enums'
import { debounce, openNewTab } from '~lib/utils'
import type { Group, Project } from '~lib/types'
import { useStorage } from '~lib/useStorage'

export default function SearchBar({ setTrigger }) {
  const [subscribedProjects, setSubscribedProjects] = useStorage('subscribedProjects', [])
  const [subscribedGroups, setSubscribedGroups] = useStorage('subscribedGroups', [])
  const [excludedProjects, setExcludedProjects] = useStorage('excludedProjects', [])
  const [excludedGroups, setExcludedGroups] = useStorage('excludedGroups', [])
  const [searchQuery, setSearchQuery] = useState('')
  const [projectResults, setProjectResults] = useState([])
  const [groupResults, setGroupResults] = useState([])

  const [isLoading, setIsLoading] = useState(false)

  const fetchProjects = async (query: string) => {
    if (!query) {
      setProjectResults([])
      setGroupResults([])
      return
    }
    setIsLoading(true)
    try {
      const response = await sendToBackground({
        // @ts-ignore
        name: ACTIONS.SEARCH,
        body: {
          query
        }
      })
      if (response.message.success) {
        setProjectResults(response.message.results.projects)
        setGroupResults(response.message.results.groups)
      } else {
        addToast({ title: 'Error fetching items', description: response.message.error, color: 'warning' })
      }
    } catch (error) {
      console.error('Error fetching items:', error)
    } finally {
      setIsLoading(false)
    }
  }

  const debouncedFetchProjects = useCallback(debounce(fetchProjects, 300), [])

  const handleSearchChange = (query: string) => {
    setSearchQuery(query)
    debouncedFetchProjects(query) // Use the debounced function
  }

  const getListOfInterest = (isProject, isSubscription) => {
    let listOfInterest, setListOfInterest

    if (isProject) {
      if (isSubscription) {
        listOfInterest = subscribedProjects
        setListOfInterest = setSubscribedProjects
      } else {
        listOfInterest = excludedProjects
        setListOfInterest = setExcludedProjects
      }
    } else {
      if (isSubscription) {
        listOfInterest = subscribedGroups
        setListOfInterest = setSubscribedGroups
      } else {
        listOfInterest = excludedGroups
        setListOfInterest = setExcludedGroups
      }
    }

    return [listOfInterest, setListOfInterest]
  }

  const handleAdd = (itemId: number, isProject, isSubscription) => {
    // Determine which list and setter to use based on the parameters
    let [listOfInterest, setListOfInterest] = getListOfInterest(isProject, isSubscription)
    let [oppositeListOfInterest, setOppositeListOfInterest] = getListOfInterest(isProject, !isSubscription)
    const resultsOfInterest = isProject ? projectResults : groupResults

    const selectedProject = resultsOfInterest.find((item: Project | Group) => item.id == itemId)

    if (selectedProject) {
      const isDuplicate = listOfInterest.some((item: Project | Group) => item.id === selectedProject.id)
      if (!isDuplicate) {
        setListOfInterest([...listOfInterest, selectedProject])
        const updatedOppositeList = oppositeListOfInterest.filter((item: Project | Group) => item.id !== selectedProject.id);
        setOppositeListOfInterest(updatedOppositeList);
      }
    }
    setSearchQuery('')
    setProjectResults([])
    setGroupResults([])
    setTrigger((prev: number) => prev + 1)
  }

  const handleRemove = (itemId: number, isProject: boolean, isSubscription: boolean) => {
    let [listOfInterest, setListOfInterest] = getListOfInterest(isProject, isSubscription)
    const subs: Project[] | Group[] = listOfInterest.filter((i: Project | Group) => i.id !== itemId)
    setListOfInterest(subs)
    setTrigger((prev: number) => prev + 1)
  }

  const handleIsSelected = (itemID: number, isProject: boolean, isSubscription: boolean) => {
    let [listOfInterest, setListOfInterest] = getListOfInterest(isProject, isSubscription)

    return listOfInterest.some((item: Project | Group) => item.id === itemID)
  }

  const autoCompleteWrapper = (record: Project | Group, isProject: boolean) => (
    <AutocompleteItem
      variant="faded"
      hideSelectedIcon
      isReadOnly
      key={record.id}
      classNames={{
        base: 'flex items-center max-w-full', // Ensure the base container uses flex
        title: 'truncate flex-1 min-w-0', // Allow the title to shrink and truncate
        description: 'truncate flex-1 min-w-0', // Allow the description to shrink and truncate
        wrapper: 'flex-1 min-w-0'
      }}
      endContent={
        <ButtonGroup className="flex-shrink-0 ml-2">
          <Button
            variant="ghost"
            size="sm"
            onPress={() => handleAdd(record.id, isProject, true)}
            color={handleIsSelected(record.id, isProject, true) ? 'primary' : 'default'}>
            subscribe
          </Button>
          <Button
            variant="ghost"
            size="sm"
            onPress={() => handleAdd(record.id, isProject, false)}
            color={handleIsSelected(record.id, isProject, false) ? 'primary' : 'default'}>
            exclude
          </Button>
        </ButtonGroup>
      }
      description={
        'name_with_namespace' in record
          ? record.name_with_namespace
          : record.description
            ? record.description
            : record.path
      }>
      {record.name}
    </AutocompleteItem>
  )

  const listBoxItemWrapper = (record: Project | Group, isProject: boolean, isSubscription: boolean) => (
    <ListboxItem
      key={record.id}
      variant="shadow"
      aria-label={`subscribed`}
      description={
        'name_with_namespace' in record
          ? record.name_with_namespace
          : record.description
            ? record.description
            : record.path
      }
      title={record.name}
      startContent={
        <Avatar
          showFallback
          size="sm"
          name={record.name && record.name.length > 0 && record.name[0]}
          src={'avatar_url' in record ? record.avatar_url : null}
          onClick={(e) => openNewTab(e.target, 'web_url' in record ? record.web_url : null, 'project_url')}
        />
      }
      endContent={
        <Button
          variant="light"
          size="sm"
          isIconOnly
          onPress={() => handleRemove(record.id, isProject, isSubscription)}>
          X
        </Button>
      }
    />
  )

  return (
    <>
      <Autocomplete
        aria-label="autocomplete search for items"
        labelPlacement="outside"
        label="Subscribe or exclude specific projects or groups"
        placeholder="Search project/group or enter project/group id"
        variant="bordered"
        size="md"
        items={[...projectResults, ...groupResults]}
        defaultItems={[...projectResults, ...groupResults]}
        isLoading={isLoading}

        onInputChange={handleSearchChange}
        inputValue={searchQuery || ''}>
        {projectResults.length > 0 && (
          <AutocompleteSection title="Projects" items={projectResults} showDivider>
            {(record: Project | Group) => {
              return autoCompleteWrapper(record, true)
            }}
          </AutocompleteSection>
        )}
        {groupResults.length > 0 && (
          <AutocompleteSection title="Groups" items={groupResults} showDivider>
            {(record: Project | Group) => {
              return autoCompleteWrapper(record, false)
            }}
          </AutocompleteSection>
        )}
      </Autocomplete>
      <Spacer y={3} />

      <Listbox
        color="primary"
        hideEmptyContent
        aria-label="list of subs and excluded project and groups"
        selectionMode="none"
        variant="solid"
        itemClasses={{ description: 'max-w-[300px]' }}>
        {subscribedProjects.length > 0 && (
          <ListboxSection title="Subscribed Projects" items={subscribedProjects} showDivider>
            {(record: Project | Group) => listBoxItemWrapper(record, true, true)}
          </ListboxSection>
        )}
        {subscribedGroups.length > 0 && (
          <ListboxSection title="Subscribed Groups" items={subscribedGroups} showDivider>
            {(record: Project | Group) => listBoxItemWrapper(record, false, true)}
          </ListboxSection>
        )}
        {excludedProjects.length > 0 && (
          <ListboxSection title="Excluded Projects" items={excludedProjects} showDivider>
            {(record: Project | Group) => listBoxItemWrapper(record, true, false)}
          </ListboxSection>
        )}
        {excludedGroups.length > 0 && (
          <ListboxSection title="Excluded Groups" items={excludedGroups} showDivider>
            {(record: Project | Group) => listBoxItemWrapper(record, false, false)}
          </ListboxSection>
        )}
      </Listbox>
    </>
  )
}
