import React from 'react'
import {Avatar, AvatarGroup, Tooltip} from "@heroui/react"
import type {Approver} from "~lib/types";

export default function ApprovedUsers({approvedUsers}) {
    return (
        approvedUsers && (
            <AvatarGroup size="sm" color="success" isBordered>
                {approvedUsers.map((user: Approver) => (
                    <Tooltip key={user.user.name} size="sm" delay={200} closeDelay={0} content={user.user.username}>
                        <Avatar showFallback src={user.user.avatar_url}/>
                    </Tooltip>
                ))}
            </AvatarGroup>
        )
    )
}
