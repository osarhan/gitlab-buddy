import React from 'react'

import {Button, Tooltip} from "@heroui/react"
import {
    Canceled,
    Closed,
    Failed,
    Manual,
    NotFound,
    Open,
    Pending,
    Preparing,
    Running,
    Scheduled,
    Skipped,
    Success,
    Warning
} from '~lib/statusIcons'
import {openNewTab} from '~lib/utils'
import {useTheme} from "next-themes";

const height = '24'
const width = '24'
const white = '#000'
const black = '#fff'

export default function Pipeline({pipeline_data}) {
    const { theme } = useTheme()

    const icons = {
        status_success: <Success h={height} w={width} color={theme === 'dark' ? white : black}/>,
        status_canceled: <Canceled h={height} w={width} color={theme === 'dark' ? white : black}/>,
        status_closed: <Closed h={height} w={width} color={theme === 'dark' ? white : black}/>,
        status_failed: <Failed h={height} w={width} color={theme === 'dark' ? white : black}/>,
        status_manual: <Manual h={height} w={width} color={theme === 'dark' ? white : black}/>,
        status_notfound: <NotFound h={height} w={width} color={theme === 'dark' ? white : black}/>,
        status_open: <Open h={height} w={width} color={theme === 'dark' ? white : black}/>,
        status_pending: <Pending h={height} w={width} color={theme === 'dark' ? white : black}/>,
        status_preparing: <Preparing h={height} w={width} color={theme === 'dark' ? white : black}/>,
        status_running: <Running h={height} w={width} color={theme === 'dark' ? white : black}/>,
        status_scheduled: <Scheduled h={height} w={width} color={theme === 'dark' ? white : black}/>,
        status_skipped: <Skipped h={height} w={width} color={theme === 'dark' ? white : black}/>,
        status_warning: <Warning h={height} w={width} color={theme === 'dark' ? white : black}/>
    }
    return (
        <Tooltip placement="right" closeDelay={0} content={'Pipeline: ' + pipeline_data?.detailed_status?.label}>
            <Button onPress={(e) => openNewTab(e, pipeline_data.web_url, 'pipeline')} size="sm" variant="light" isIconOnly>
                <div className="fill-green-700">{icons[pipeline_data?.detailed_status?.icon]}</div>
            </Button>
        </Tooltip>
    )
}
