import React, { useState } from 'react'
import { Avatar, Card, CardBody, Divider, Link, Spacer, Button, Tooltip, Chip, addToast } from '@heroui/react'
import { ArrowPath, Conflict, Folder, XMark } from '~lib/icons'
import dayjs from 'dayjs'
import relativeTime from 'dayjs/plugin/relativeTime'
import ApprovedUsers from './ApprovedUsers'
import Pipeline from './Pipeline'
import { openNewTab } from '~lib/utils'
import Comments from './Comments'
import { useTheme } from 'next-themes'
import type {Label} from "~lib/types";


dayjs.extend(relativeTime)

const VIEW_STATUS = {
  not_approved: 'bg-orange-500',
  approved: 'bg-green-500',
  merged: 'bg-blue-500',
  created: 'bg-yellow-500'
}

export default function MRCard({ mr, handleHideMR, status, issueWebsitePrefix }) {
  const [hidden, setHidden] = useState(false)
  const opened = mr.state === 'opened'
  const { theme } = useTheme()

  const handleIssueChipClick = (e) => {
    if (mr.issues?.length > 0) {
      if (mr.issues[0].hasOwnProperty('web_url')) {
        openNewTab(e, mr.issues[0].web_url, 'issue')
      } else if (issueWebsitePrefix) {
        let url = `${issueWebsitePrefix}${mr.issues[0].id}`
        openNewTab(e, url, 'issue')
      } else {
        addToast({
          title: 'External tracker url not defined',
          description: 'Add your external issue tracker (eg jira) instance url to Lab Partner in settings.'
        })
      }
    }
  }

  return (
    <Card shadow="lg" radius="sm" className="overflow-x-hidden font-sans">
      <div className={`absolute left-0 top-0 h-full w-1 ${VIEW_STATUS[status]}`} />
      <CardBody>
        <div className="flex flex-row gap-3">
          <div className="ml-2 flex flex-col">
            {opened && (
              <Button
                size="sm"
                isIconOnly
                isDisabled={hidden}
                variant="light"
                onPress={() => {
                  handleHideMR(mr)
                  setHidden(true)
                }}>
                {hidden ? <ArrowPath /> : <XMark color={theme === 'dark' ? '#fff' : '#000'} />}
              </Button>
            )}
            <Spacer y={2} />
            <div className="items-center">
              <Tooltip size="sm" delay={200} closeDelay={0} content={mr.author.username}>
                <Button
                  isIconOnly
                  variant="light"
                  onPress={(e) => openNewTab(e, mr.author.web_url, 'author')}>
                  <Avatar showFallback radius="full" size="md" src={mr.author.avatar_url} />
                </Button>
              </Tooltip>
            </div>
          </div>

          <div className="flex flex-col">
            <Link
              className="truncate"
              isBlock
              color="primary"
              onPress={(e) => openNewTab(e, mr.web_url, 'title')}>
              {mr.title}
            </Link>

            <div className="flex flex-row gap-1">
              {mr.issues?.length > 0 && (
                <Chip
                  size="sm"
                  variant="bordered"
                  color="default"
                  onClick={(event) => handleIssueChipClick(event)}>
                  {mr.issues[0].id}
                </Chip>
              )}
              <Button
                size="sm"
                startContent={<Folder />}
                variant="light"
                color="default"
                onPress={(e) => openNewTab(e, mr.project_url, 'project')}
                className="text-wrap">
                {mr.project_name}
              </Button>
              <Button className="text-wrap text-start" size="sm" variant="light" color="default" isDisabled>
                {mr.target_branch}
              </Button>
            </div>

            <div className="flex flex-row gap-1 text-gray-400 text-xs font-light">
              <p>{mr.mrType}</p>
              <Divider orientation="vertical" />
              <p>Created {dayjs(mr.created_at).fromNow()}</p>
              <Divider orientation="vertical" />
              <p>Updated {dayjs(mr.updated_at).fromNow()}</p>
            </div>
            {opened && <Spacer y={2} />}

            {opened && (
              <div>
                <div className="flex flex-row justify-between max-w-[300px]">
                  <div className="flex">
                    {mr.draft && (
                      <Tooltip content="Draft" placement="right">
                        <Button isDisabled variant="bordered" size="sm" radius="full">
                          Draft
                        </Button>
                      </Tooltip>
                    )}

                    {mr.has_conflicts && (
                      <Tooltip closeDelay={0} content="Merge Conflict">
                        <Button isDisabled variant="flat" size="sm" radius="full" color="danger" isIconOnly>
                          <Conflict />
                        </Button>
                      </Tooltip>
                    )}

                    <Comments comments={mr.discussions} url={mr.web_url} project_url={mr.project_url} />
                    {mr.pipeline_data && <Pipeline pipeline_data={mr.pipeline_data} />}
                  </div>
                  <div className="flex justify-end">
                    <ApprovedUsers approvedUsers={mr.approved_by} />
                  </div>
                </div>
                {mr.labels && mr.labels.length > 0 && <Spacer y={1} />}
                <div className="flex flex-row">
                  {mr.labels &&
                    mr.labels.length > 0 &&
                    mr.labels.map((label: Label) => {
                      return (
                        <Chip
                          key={label.id}
                          size="sm"
                          style={{ backgroundColor: label.color, color: label.text_color }}>
                          {label.name}
                        </Chip>
                      )
                    })}
                </div>
              </div>
            )}
          </div>
        </div>
      </CardBody>
    </Card>
  )
}
