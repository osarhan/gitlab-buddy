import { Button, Card, CardBody, CardFooter, Input, Spacer, Switch, Tab, Tabs } from '@heroui/react'
import React from 'react'
import { EyeFilledIcon, EyeSlashFilledIcon, MoonIcon, SunIcon } from '~lib/icons'
import { getUrlSchemas, openNewTab } from '~lib/utils'
import Footer from './Footer'
import { useTheme } from 'next-themes'
import { sendEvent } from '~lib/analytics'

export default function Login({
  gitlabUrl,
  setGitlabUrl,
  userToken,
  setUserToken,
  resetApplication,
  cookie,
  setCookie,
    login
}) {
  const [isVisible, setIsVisible] = React.useState(false)
  const [selected, setSelected] = React.useState('token')
  const { theme, setTheme } = useTheme()

  const toggleVisibility = () => setIsVisible(!isVisible)
  return (
    <div className="font-sans text-medium">
      <div className="flex flex-row justify-between">
        <p className="flex text-2xl font-bold">Welcome to Lab Partner</p>
        <Switch
          className="flex"
          isSelected={theme === 'dark'}
          size="sm"
          color="primary"
          onChange={(e) => {
            e.target.checked ? setTheme('dark') : setTheme('light')
          }}
          thumbIcon={({ isSelected, className }) =>
            isSelected ? <SunIcon className={className} /> : <MoonIcon className={className} />
          }></Switch>
      </div>
      <Spacer y={3} />
      <div>
        <div>
          <p>Authorize Lab Partner to access gitlab in two different ways.</p>
          <Spacer y={2} />
          <p className="font-bold">1. Personal Access Token</p>
          <p>
            Provide Lab Partner with your personal access token. When setting up within your gitlab account,
            check "read scope".
          </p>
          <Spacer y={2} />
          <p className="font-bold">2. Browser Session Token</p>
          <p>
            Allow Lab Partner to access your browser session token. Leave personal access token empty. (Active
            gitlab session required)
          </p>
          <Spacer y={2} />
          <p className="font-thin">For consistent background refresh, option 1 is recommended.</p>
        </div>
        <Spacer y={3} />
        <Input
          onChange={(e) => setGitlabUrl(e.target.value)}
          onClear={() => setGitlabUrl(null)}
          label="Gitlab Url"
          type="url"
          value={gitlabUrl}
          variant="bordered"
          placeholder="Enter the url of your gitlab instance"
          description="This is the url of your gitlab instance. e.g. https://www.gitlab.com"
          isClearable
          isRequired
        />
        <Spacer y={2} />

        <Tabs
          aria-label="Options"
          selectedKey={selected}
          fullWidth
          onSelectionChange={(key: any) => setSelected(key)}>
          <Tab key="token" title="Personal Access Token">
            <Card>
              <CardBody>
                <Input
                  onChange={(e) => setUserToken(e.target.value)}
                  label="Personal Access Token"
                  variant="bordered"
                  placeholder="Enter your personal access token"
                  endContent={
                    <Button isIconOnly variant='light' onPress={toggleVisibility}>
                      {isVisible ? (
                        <EyeSlashFilledIcon className="text-2xl text-default-400 pointer-events-none" />
                      ) : (
                        <EyeFilledIcon className="text-2xl text-default-400 pointer-events-none" />
                      )}
                    </Button>
                  }
                  type={isVisible ? 'text' : 'password'}
                  description={
                    <div className="flex flex-row">
                      <p>Generate a</p>
                      <Spacer />
                      <a
                        className="text-blue-500 cursor-pointer"
                        onClick={(e) =>
                          openNewTab(
                            e,
                            'https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#create-a-personal-access-token',
                              'pat_instructions'
                          )
                        }>
                        Personal Access Token
                      </a>
                      <Spacer />
                      <p>profile settings of your gitlab account</p>
                    </div>
                  }
                />
              </CardBody>
              <CardFooter>
                <Button
                  color="primary"
                  isDisabled={!gitlabUrl || !userToken}
                  fullWidth
                  onPress={login}>
                  Submit
                </Button>
              </CardFooter>
            </Card>
          </Tab>
          <Tab key="session" title="Browser Session Token">
            <Card>
              <CardBody>
                <Button
                  color={cookie ? 'success' : 'secondary'}
                  size="sm"
                  isDisabled={cookie || !gitlabUrl}
                  variant="bordered"
                  onPress={() => {
                    const url = getUrlSchemas(gitlabUrl)
                    chrome.permissions.request(
                      {
                        permissions: ['cookies'],
                        origins: [url.GITLAB_COOKIE_SCHEME]
                      },
                      (grant) => setCookie(grant)
                    )
                  }}>
                  {cookie ? 'Permissions Granted' : 'Grant Permissions'}
                </Button>
              </CardBody>
              <CardFooter>
                <Button
                  color="primary"
                  isDisabled={!gitlabUrl || !cookie}
                  fullWidth
                  onPress={() => {
                    login()
                    setUserToken('')
                    void sendEvent(userToken ? 'sessionToken' : 'PAT', {})
                  }}>
                  Submit
                </Button>
              </CardFooter>
            </Card>
          </Tab>
        </Tabs>
      </div>

      <Spacer y={2} />
      <Footer gitlabUrl={gitlabUrl} resetApplication={resetApplication} />
    </div>
  )
}
