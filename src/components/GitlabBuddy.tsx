import React, { useEffect, useState } from 'react'
import { sendToBackground } from '@plasmohq/messaging'
import {
  addToast,
  Button,
  Modal,
  ModalContent,
  ModalHeader,
  Progress,
  Spacer,
  useDisclosure
} from '@heroui/react'

import { useStorage } from '~lib/useStorage'
import { ACTIONS, VIEWS, INIT_STATE, CATEGORIES } from '~lib/enums'
import NavBar from './NavBar'
import Login from './Login'
import MRCard from './MRCard'
import { sendEvent } from '~lib/analytics'
import type { MergeRequestData } from '~lib/types'
import { getUrlSchemas } from '~lib/utils'
import Settings from '~components/Settings'

function GitlabBuddy() {
  const [loading, setLoading] = useState(false)
  const [trigger, setTrigger] = useState(0)
  const [refreshDateStamp, setRefreshDateStamp] = useStorage('refreshDateStamp', 0)
  const [isAuth, setIsAuth] = useStorage('isAuth', false)
  const [mergeRequests, setMergeRequests] = useStorage('mergeRequests', INIT_STATE)
  const [cookie, setCookie] = useStorage('cookie', false)
  const [alarm, setAlarm] = useStorage('alarm', false)

  // user settings
  const [hiddenMRs, setHiddenMRs] = useStorage('hiddenMRs', [])
  const [icon, setIcon] = useStorage('icon', true)
  const [iconColor, setIconColor] = useStorage('iconColor', '#F00')
  const [bgRefreshInterval, setBgRefreshInterval] = useStorage('bgRefreshInterval', '0')
  const [mergedDaysLookBack, setMergedDaysLookBack] = useStorage('mergedDaysLookBack', 2)
  const [issueWebsitePrefix, setIssueWebsitePrefix] = useStorage('issueWebsitePrefix', null)

  const [gitlabUrl, setGitlabUrl] = useStorage('gitlabUrl', undefined)
  const [userToken, setUserToken] = useStorage('userToken', null)

  const { isOpen, onOpen, onOpenChange } = useDisclosure()

  const resetApplication = () => {
    setIsAuth(false)
    setGitlabUrl(undefined)
    setCookie(false)
    setAlarm(false)
    setMergeRequests(INIT_STATE)
    void chrome.permissions.remove({
      permissions: ['cookies']
    })
    void chrome.storage.local.clear()
  }

  const handleHideMR = (mr: MergeRequestData) => {
    void sendEvent('hide_mr', {})
    const list = [...hiddenMRs]

    if (!list.some((hiddenMR) => hiddenMR.id === mr.id)) {
      const obj = { id: mr.id, date: Date.now() }
      list.push(obj)

      setTrigger((prev) => prev + 1)
      setHiddenMRs(list)
      addToast({
        title: `${mr.author.name}'s MR is now hidden.`,
        description: 'If there is an update on the MR, it will reappear on your list.',
        hideCloseButton: true,
        timeout:10000,
        priority: 1,
        endContent: (
          <Button size="sm" variant='ghost' color='primary' onPress={() => undoAddToHiddenMRs(mr.id)}>
            Undo
          </Button>
        )
      })
    }
  }

  const undoAddToHiddenMRs = (id: number) => {
    void sendEvent('undo_hide_mr', {})
    // Create a new list by filtering out the MR with the specified ID
    const list = hiddenMRs.filter((mr) => mr.id !== id)

    // Update the hidden MRs list
    setHiddenMRs(list)
    setTrigger((prev) => prev + 1)
  }

  async function login() {
    setLoading(true)
    const resp = await sendToBackground({
      // @ts-ignore
      name: ACTIONS.LOGIN
    })
    setLoading(false)
    const { message } = resp
    if (message.success) {
      setIsAuth(true)
      void getMergeRequests()
    } else {
      addToast({
        title: 'Failed logging in',
        description: 'Please check VPN, Url, Token, or login to Gitlab to refresh browser cookie',
        color: 'danger',
        hideCloseButton: true,
        timeout: 10000
      })
    }
  }

  async function getMergeRequests(triggered: number = 0) {
    setLoading(true)
    const resp = await sendToBackground({
      // @ts-ignore
      name: ACTIONS.MERGE_REQUESTS
    })
    setLoading(false)

    const { message } = resp
    if (message.success) {
      setMergeRequests(message.mergeRequests)
      setIsAuth(true)
      setRefreshDateStamp(Date.now())
      if (!!triggered) {
        addToast({
          color: 'success',
          title: 'Refreshed Merge Requests!',
          description: `${String.fromCodePoint(0x1f680)} Refreshed in ${Math.trunc(message.time)}ms! ${String.fromCodePoint(0x1f680)}`,
          priority:10
        })
      }
    } else {
      console.log(message.error)
      gitlabUrl &&
        addToast({
          title: `Fetching merge requests failed: ${message.error.name}`,
          description: 'Check VPN, Url, Token, or login to Gitlab to refresh browser cookie',
          color: 'danger',
          hideCloseButton: true,
          timeout: 10000
        })
    }
  }

  async function updateRefreshInterval(refresh: string) {
    setBgRefreshInterval(refresh)
    void (await sendToBackground({
      // @ts-ignore
      name: ACTIONS.UPDATE_REFRESH
    }))
  }

  useEffect(() => {
    const listener = (message) => {
      if (message.success) {
        setMergeRequests(message.mergeRequests)
        setRefreshDateStamp(Date.now())
      }
    }
    const fetchGitlab = async () => {
      await getMergeRequests(trigger)
    }

    if (!cookie || !alarm) {
      chrome.permissions.getAll().then((resp) => {
        setCookie(
          !!(
            gitlabUrl &&
            resp.origins.length > 0 &&
            resp.origins.some((str) => str.includes(getUrlSchemas(gitlabUrl).GITLAB_COOKIE_SCHEME)) &&
            resp.permissions.includes('cookies')
          )
        )
        setAlarm(resp.permissions.includes('alarms'))
      })
    }

    chrome.runtime.onMessage.addListener(listener)
    void fetchGitlab()
    return () => {
      chrome.runtime.onMessage.removeListener(listener)
    }
  }, [trigger])

  useEffect(() => {
    window.dataLayer = window.dataLayer || []
    window.gtag = function gtag() {
      window.dataLayer.push(arguments) // eslint-disable-line
    }
    window.gtag('js', new Date())
    window.gtag('config', process.env.PLASMO_PUBLIC_GTAG_ID, {
      page_path: '/sidepanel',
      debug_mode: true
    })

    window.gtag('event', 'login', {
      method: 'TEST'
    })
  }, [])

  return (
    <div className="min-h-screen font-sans text-medium">
      <Progress
        size="sm"
        aria-label="progress bar"
        radius="none"
        isIndeterminate={loading}
        disableAnimation={!loading}
        value={!loading && 100}
      />
      <NavBar
        gitlabUrl={gitlabUrl}
        isAuth={isAuth}
        onOpen={onOpen}
        setTrigger={setTrigger}
        loading={loading}
        refreshedDate={refreshDateStamp}
      />
      <div className="px-2">
        {isAuth ? (
          <div>
            {Object.values(VIEWS).map((v) => {
              return (
                <div key={v}>
                  <p className="flex flex-row justify-start text-xl font-bold">
                    {v === VIEWS.MERGED && Number(mergedDaysLookBack) === 0
                      ? null
                      : v === VIEWS.MERGED
                        ? `${CATEGORIES[v].label} (last ${mergedDaysLookBack} day${mergedDaysLookBack > 1 ? 's' : ''})`
                        : CATEGORIES[v].label}
                  </p>
                  {mergeRequests[v].length === 0 &&
                    !(v === VIEWS.MERGED && Number(mergedDaysLookBack) === 0) && (
                      <p>{CATEGORIES[v]['emptyListText']}</p>
                    )}

                  {mergeRequests[v].map((mr: MergeRequestData) => {
                    return (
                      <div className="py-1" key={mr.id}>
                        <MRCard
                          mr={mr}
                          handleHideMR={handleHideMR}
                          status={v}
                          issueWebsitePrefix={issueWebsitePrefix}
                        />
                      </div>
                    )
                  })}
                  <Spacer y={3} />
                </div>
              )
            })}
          </div>
        ) : (
          <Login
            setGitlabUrl={setGitlabUrl}
            gitlabUrl={gitlabUrl}
            setUserToken={setUserToken}
            userToken={userToken}
            resetApplication={resetApplication}
            cookie={cookie}
            setCookie={setCookie}
            login={login}
          />
        )}
      </div>
      <Modal
        isOpen={isOpen}
        size="xl"
        scrollBehavior="inside"
        onOpenChange={() => {
          onOpenChange()
          void sendEvent('settings', {})
        }}>
        <ModalContent>
          {(onClose) => (
            <>
              <ModalHeader className="text-xl flex flex-col">Settings</ModalHeader>
              <Settings
                icon={icon}
                setIcon={setIcon}
                iconColor={iconColor}
                setIconColor={setIconColor}
                hiddenMRs={hiddenMRs}
                setHiddenMRs={setHiddenMRs}
                resetApplication={resetApplication}
                bgRefreshInterval={bgRefreshInterval}
                updateRefreshInterval={updateRefreshInterval}
                gitlabUrl={gitlabUrl}
                alarm={alarm}
                setAlarm={setAlarm}
                setTrigger={setTrigger}
                issueWebsitePrefix={issueWebsitePrefix}
                setIssueWebsitePrefix={setIssueWebsitePrefix}
                mergedDaysLookBack={mergedDaysLookBack}
                setMergedDaysLookBack={setMergedDaysLookBack}
              />
            </>
          )}
        </ModalContent>
      </Modal>
    </div>
  )
}

export default GitlabBuddy
