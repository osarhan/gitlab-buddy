import {
  Button,
  Divider,
  Link,
  Modal,
  ModalBody,
  ModalContent,
  ModalHeader,
  Spacer,
  User,
  useDisclosure
} from '@heroui/react'
import dayjs from 'dayjs'
import MarkdownIt from 'markdown-it'
import { full as emoji } from 'markdown-it-emoji'
import relativeTime from 'dayjs/plugin/relativeTime'
import { CommentsIcon } from '~lib/icons'
import { openNewTab } from '~lib/utils'
import { sendEvent } from '~lib/analytics'

dayjs.extend(relativeTime)

// Set options
const md = MarkdownIt({
  linkify: true,
  typographer: true,
  image: true
}).use(emoji)

export function replaceImageUrl(content: string, imageUrlPrefix: string) {
  // Regex pattern to match the image tag format ![image](/uploads/e0c55f4177d1d6236d4846963ae81118/image.png)
  const imageRegex = /!\[image]\((.*?)\)/g
  return content.replace(imageRegex, (match: string, imageUrl: string) => {
    const updatedUrl = `${imageUrlPrefix}${imageUrl}`
    return `![image](${updatedUrl})`
  })
}

export default function Comments({ comments, url, project_url }) {
  const renderMarkdown = (content) => {
    const updatedContent = replaceImageUrl(content, project_url)
    return { __html: md.render(updatedContent) }
  }
  const { isOpen, onOpen, onOpenChange } = useDisclosure()

  function updated(date) {
    return dayjs(date).fromNow()
  }

  function getNotes(comments) {
    if (!comments) {
      return []
    }
    const res = []
    comments.forEach((c) => {
      const discussion = []
      if (c.notes && c.notes.length > 0) {
        const { notes } = c
        notes.forEach((note) => {
          if ((!note.system && !note.resolvable) || (note.resolvable && !note.resolved)) {
            discussion.push(note)
          }
        })
      }
      if (discussion.length > 0) res.push(discussion)
    })
    return res
  }

  const displayComments = () => {
    const notes = getNotes(comments)
    return notes.map((n, index) => {
      return (
        <div key={index}>
          <Divider />
          <Spacer y={3} />
          {n.map((c, c_index) => {
            return (
              <div key={c_index} className={c_index !== 0 ? 'ps-3' : ''}>
                <div>
                  <User
                    name={c.author.name}
                    description={
                      <Link
                        onPress={(e) => openNewTab(e, `${url}#note_${c.id}`, 'comment')}
                        isExternal
                        size="sm">
                        {updated(c.created_at)}
                      </Link>
                    }
                    avatarProps={{ src: c.author.avatar_url }}
                  />
                </div>
                <div className="ps-2 text-md" dangerouslySetInnerHTML={renderMarkdown(c.body)} />
              </div>
            )
          })}
        </div>
      )
    })
  }

  return (
    getNotes(comments).length > 0 && (
      <div>
        <Button
          onPress={() => {
            onOpen()
            void sendEvent('comments', {})
          }}
          size="sm"
          variant="light"
          endContent={<p>{getNotes(comments).length}</p>}>
          <CommentsIcon />
        </Button>
        <Modal
          shouldBlockScroll
          scrollBehavior="inside"
          isOpen={isOpen}
          size="lg"
          className={`text-foreground bg-background border border-white`}
          onOpenChange={onOpenChange}>
          <ModalContent>
            {(onClose) => (
              <>
                <ModalHeader className="flex flex-col gap-1">Comments</ModalHeader>
                <ModalBody>{displayComments()}</ModalBody>
              </>
            )}
          </ModalContent>
        </Modal>
      </div>
    )
  )
}
