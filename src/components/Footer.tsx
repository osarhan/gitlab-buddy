import React from 'react'
import { Button, Card, CardBody, CardHeader, Divider, Spacer } from '@heroui/react'
import { openNewTab } from '~lib/utils'
import { sendEvent } from '~lib/analytics'
import { GitLab } from '~lib/icons'

export default function Footer({ gitlabUrl, resetApplication }) {
  return (
    <div>
      <Card shadow="sm" radius="sm">
        <CardHeader className="text-lg">Feedback / Report Bugs</CardHeader>
        <CardBody>
          <div className="">
            <p>
              If you have feedback and/or bug reports, they are welcomed and encouraged!. Please open an issue
              in the gitlab repository.
            </p>
          </div>

          <Button
              startContent={<GitLab/>}
            variant="faded"
            onPress={(e) => openNewTab(e, 'https://gitlab.com/osarhan/lab-partner', 'extension')}>
            Lab Partner Project Repository
          </Button>
        </CardBody>
      </Card>
      <Spacer y={2} />
      <Card shadow="sm" radius="sm">
        <CardBody>
          <Button
            color="danger"
            variant="solid"
            onPress={() => {
              resetApplication()
              void sendEvent('reset', {})
            }}>
            Reset Lab Partner
          </Button>
        </CardBody>
      </Card>

      <Divider />
      <div className="mx-5 flex flex-row justify-between">
        <span>Version: {chrome.runtime.getManifest().version}</span>
        <span>{gitlabUrl}</span>
      </div>
    </div>
  )
}
