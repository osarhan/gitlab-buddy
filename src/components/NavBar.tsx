import { Button, Navbar, NavbarBrand, NavbarItem, NavbarContent } from '@heroui/react'

import Logo from 'react:~/assets/icon.svg'
import { Cog } from '~lib/icons'
import { getUrlSchemas, openNewTab } from '~lib/utils'
import React from 'react'
import RefreshBar from '~components/RefreshBar'

function NavBar({ gitlabUrl, isAuth, onOpen, loading, setTrigger, refreshedDate }) {
  return (
    <Navbar isBordered>
      <NavbarBrand>
        <Logo
          aria-label="Logo"
          className="h-10 w-10"
          onClick={(e) => openNewTab(e, getUrlSchemas(gitlabUrl).GITLAB_BASE_URL_HTTP, 'logo')}
        />
        <span className="pt-2 text-xl font-bold bg-gradient-to-tr from-[#ec7eb3] to-[#2F88FF] inline-block text-transparent bg-clip-text">
          Lab Partner
        </span>
      </NavbarBrand>
      <NavbarContent justify="end">

          <NavbarItem>
            <RefreshBar isAuth={isAuth} loading={loading} setTrigger={setTrigger} refreshedDate={refreshedDate} onOpen={onOpen}/>
          </NavbarItem>

        {/*<NavbarItem>*/}
        {/*  <Button color="primary" variant="light" isIconOnly isDisabled={!isAuth} size="sm" onPress={onOpen}>*/}
        {/*    <Cog />*/}
        {/*  </Button>*/}
        {/*</NavbarItem>*/}
      </NavbarContent>
    </Navbar>
  )
}

export default NavBar
