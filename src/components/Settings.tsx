import {
  Button,
  Card,
  CardBody,
  CardHeader,
  CheckboxGroup,
  Divider,
  Input,
  Popover,
  PopoverContent,
  PopoverTrigger,
  Select,
  SelectItem,
  Spacer,
  Switch
} from '@heroui/react'
import React from 'react'
import { HexColorPicker } from 'react-colorful'
import { Clock, Merge, MoonIcon, SunIcon } from '~lib/icons'
import Footer from './Footer'
import { useTheme } from 'next-themes'
import { CustomCheckbox } from '~components/HideOptions'
import SearchBar from '~components/SearchBar'
import { sendToBackground } from '@plasmohq/messaging'
import { ACTIONS } from '~lib/enums'
import { useStorage } from '~lib/useStorage'

export default function Settings({
  icon,
  setIcon,
  iconColor,
  setIconColor,
  hiddenMRs,
  setHiddenMRs,
  resetApplication,
  bgRefreshInterval,
  updateRefreshInterval,
  gitlabUrl,
  alarm,
  setAlarm,
  setTrigger,
  issueWebsitePrefix,
  setIssueWebsitePrefix,
  mergedDaysLookBack,
  setMergedDaysLookBack
}) {
  const [viewOptions, setViewOptions] = useStorage('viewOptions', ['0', '1', '2'])
  const [hideOptions, setHideOptions] = useStorage('hideOptions', [])
  const { theme, setTheme } = useTheme()

  async function updateBadge() {
    void (await sendToBackground({
      // @ts-ignore
      name: ACTIONS.ICON_TOGGLE
    }))
  }

  return (
    <>
      <Card aria-label="card" shadow="sm" radius="sm">
        <CardBody>
          <Switch
            aria-label="switch"
            isSelected={theme === 'dark'}
            size="sm"
            color="primary"
            onChange={(e) => {
              e.target.checked ? setTheme('dark') : setTheme('light')
            }}
            thumbIcon={({ isSelected, className }) =>
              isSelected ? <SunIcon className={className} /> : <MoonIcon className={className} />
            }>
            Dark mode
          </Switch>
          <Spacer y={2} />

          <Switch
            isSelected={icon}
            size="sm"
            color="primary"
            onChange={(e) => {
              setIcon(e.target.checked)
              void updateBadge()
            }}>
            Browser Toolbar Icon Badge
          </Switch>
          <Spacer y={2} />
          {icon && (
            <div className="ps-1 flex flex-row">
              <Popover placement="right" size="sm" containerPadding={0}>
                <PopoverTrigger>
                  <Button isIconOnly size="sm" style={{ backgroundColor: iconColor }} variant="faded" />
                </PopoverTrigger>
                <PopoverContent>
                  <HexColorPicker
                    color={iconColor}
                    onChange={(newColor) => {
                      setIconColor(newColor)
                      void updateBadge()
                    }}
                  />
                </PopoverContent>
              </Popover>
              <Spacer x={2.5} />
              <p className="text-sm">Badge Color</p>
            </div>
          )}

          <Spacer y={2} />
          <Divider />
          <Spacer y={2} />
          <CardHeader className="text-lg">Merge Request Filtering and Selection</CardHeader>
          {alarm ? (
            <Select
              aria-label="select"
              label="Background Refresh"
              labelPlacement="outside"
              isDisabled={!alarm}
              variant="bordered"
              selectedKeys={[bgRefreshInterval]}
              defaultSelectedKeys={'0'}
              placeholder="Off"
              size="sm"
              fullWidth={false}
              selectionMode="single"
              startContent={<Clock />}
              disabledKeys={[bgRefreshInterval]}
              onChange={(e) => {
                updateRefreshInterval(e.target.value)
              }}>
              <SelectItem key="0">Off</SelectItem>
              <SelectItem key="1">One Minute Interval</SelectItem>
              <SelectItem key="5">Five Minute Interval</SelectItem>
              <SelectItem key="15">Fifteen Minute Interval</SelectItem>
              <SelectItem key="30">Thirty Minute Interval</SelectItem>
            </Select>
          ) : (
            <div>
              <Button
                aria-label="button"
                color="secondary"
                size="sm"
                variant="bordered"
                onPress={() =>
                  chrome.permissions.request(
                    {
                      permissions: ['alarms']
                    },
                    (grant) => setAlarm(grant)
                  )
                }>
                Grant Permissions for Background Refresh
              </Button>
            </div>
          )}
          <Spacer y={2} />
          <Select
            labelPlacement="outside"
            label="Merged MR lookback Days"
            variant="bordered"
            selectedKeys={[mergedDaysLookBack && mergedDaysLookBack.toString()]}
            size="sm"
            fullWidth={false}
            selectionMode="single"
            startContent={<Merge />}
            disabledKeys={[mergedDaysLookBack]}
            onChange={(e) => {
              setMergedDaysLookBack(e.target.value)
              setTrigger((prev: number) => prev + 1)
            }}>
            <SelectItem key="0">Hide merged merge requests</SelectItem>
            <SelectItem key="1">1 day</SelectItem>
            <SelectItem key="2">2 days</SelectItem>
            <SelectItem key="7">7 days</SelectItem>
            <SelectItem key="14">14 days</SelectItem>
          </Select>

          <Spacer aria-hidden y={4} />
          <Divider aria-hidden />
          <Spacer y={4} />

          <div className="flex flex-col gap-1 w-full">
            <SearchBar setTrigger={setTrigger} />
            <CheckboxGroup
              className="gap-1"
              label="Auto Retrieve Merge Requests with:"
              orientation="horizontal"
              value={viewOptions}
              onChange={(e) => {
                setViewOptions(e)
                setTrigger((prev: number) => prev + 1)
              }}>
              <CustomCheckbox value="0">Assignee</CustomCheckbox>
              <CustomCheckbox value="1">Reviewer</CustomCheckbox>
              <CustomCheckbox value="2">Group Approval</CustomCheckbox>
            </CheckboxGroup>

            <Spacer aria-hidden y={3} />

            <CheckboxGroup
              className="gap-1"
              label="Auto Hide Merge Requests with:"
              orientation="horizontal"
              value={hideOptions}
              onChange={(e) => {
                setHideOptions(e)
                setTrigger((prev: number) => prev + 1)
              }}>
              <CustomCheckbox value="0">Draft</CustomCheckbox>
              <CustomCheckbox value="1">Broken Pipeline</CustomCheckbox>
              <CustomCheckbox value="2">Merge Conflict</CustomCheckbox>
            </CheckboxGroup>
          </div>
          <Spacer y={4} />
          <div>
            <Button
              aria-label="button"
              fullWidth
              color={!hiddenMRs || hiddenMRs.length === 0 ? 'primary' : 'secondary'}
              isDisabled={!hiddenMRs || hiddenMRs.length === 0}
              onPress={() => {
                setHiddenMRs([])
                setTrigger((prev: number) => prev + 1)
              }}>
              {hiddenMRs && hiddenMRs.length > 0
                ? `Un-hide ${hiddenMRs.length} merge request${hiddenMRs.length > 1 ? 's' : ''} `
                : 'No Hidden MRs'}
            </Button>
          </div>
          <Spacer y={2} />
          <Divider />
          <CardHeader className="text-lg">Jira / External issue trackers</CardHeader>
          <Input
            onChange={(e) => setIssueWebsitePrefix(e.target.value)}
            labelPlacement="outside"
            fullWidth
            label="Issue page url prefix"
            description="https://example.atlassion.com/browse/"
            value={issueWebsitePrefix || ''}
            placeholder="not configured"
            type="url"
            aria-label="jira external url prefix"
          />
          <Spacer y={2} />
          <Divider />
          <Footer gitlabUrl={gitlabUrl} resetApplication={resetApplication} />
        </CardBody>
      </Card>
    </>
  )
}
