import React, { useState, useEffect } from 'react'
import { Button, ButtonGroup } from '@heroui/react'
import dayjs from 'dayjs'
import relativeTime from 'dayjs/plugin/relativeTime'
import { ArrowPath, Cog } from '~lib/icons'
import { sendEvent } from '~lib/analytics'

dayjs.extend(relativeTime)

export default function RefreshBar({ isAuth, refreshedDate, loading, setTrigger, onOpen }) {
  const [fromNow, setFromNow] = useState('')

  useEffect(() => {
    if (refreshedDate > 0) {
      setFromNow(dayjs(refreshedDate).fromNow())

      const intervalId = setInterval(() => {
        setFromNow(dayjs(refreshedDate).fromNow())
      }, 1000)

      return () => clearInterval(intervalId)
    }
  }, [refreshedDate])

  return (
      <ButtonGroup size="sm" >
        {isAuth && refreshedDate > 0 && (
            <Button
                color="secondary"
                variant="light"
                startContent={<ArrowPath />}
                isDisabled={!isAuth || loading}
                onPress={() => {
                  setTrigger((prev) => prev + 1)
                  void sendEvent('refresh_mr', {})
                }}>
              {fromNow}
            </Button>
        )}
        <Button isIconOnly variant="light" color="primary" onPress={onOpen}>
          <Cog />
        </Button>
      </ButtonGroup>
  )
}