import 'https://www.googletagmanager.com/gtag/js?id=$PLASMO_PUBLIC_GTAG_ID'

import React from 'react'
import { HeroUIProvider, ToastProvider } from '@heroui/react'

import { ThemeProvider as NextThemesProvider } from 'next-themes'
import GitlabBuddy from '~components/GitlabBuddy'
import '~base.css'
import { sendEvent } from '~lib/analytics'

window.addEventListener('load', async () => {
  await sendEvent('page_view', {
    page_title: document.title,
    page_location: document.location.href
  })
})

function IndexSidePanel() {
  return (
    <HeroUIProvider>
      <NextThemesProvider attribute="class" defaultTheme="dark">
        <ToastProvider
          placement="bottom-center"
          disableAnimation
          maxVisibleToasts={1}
          toastProps={{
            color: 'primary',
            variant: 'flat',
            timeout: 3000,
            hideIcon: true,
            shouldShowTimeoutProgress: true,
            classNames: {
              closeButton: 'opacity-100 absolute right-4 top-1/2 -translate-y-1/2'
            }
          }}
        />
        <GitlabBuddy />
      </NextThemesProvider>
    </HeroUIProvider>
  )
}

export default IndexSidePanel
