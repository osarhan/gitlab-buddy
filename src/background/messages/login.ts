import type {PlasmoMessaging} from '@plasmohq/messaging'
import {login} from '~background'

const handler: PlasmoMessaging.MessageHandler = async (req, res) => {
    const message = await login().catch((e) => {
        res.send({message: {success: false, error: e.message}})
    })
    res.send({
        message
    })
}

export default handler
