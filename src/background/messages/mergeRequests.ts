import type {PlasmoMessaging} from '@plasmohq/messaging'
import getMergeRequests from '~background'

const handler: PlasmoMessaging.MessageHandler = async (req, res) => {
    const message = await getMergeRequests().catch((e) => {
        res.send({message: {success: false, error: e}})
    })
    res.send({
        message
    })
}

export default handler
