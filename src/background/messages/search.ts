import type {PlasmoMessaging} from '@plasmohq/messaging'
import {search} from '~background'

const handler: PlasmoMessaging.MessageHandler = async (req, res) => {

    const message = await search(req.body.query).catch((e) => {
        res.send({message: {success: false, error: e.message}})
    })
    res.send({
        message
    })
}

export default handler
