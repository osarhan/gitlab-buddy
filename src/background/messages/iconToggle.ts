import type {PlasmoMessaging} from '@plasmohq/messaging'
import {updateBadge} from '~background'

const handler: PlasmoMessaging.MessageHandler = async (req, res) => {
    const message = await updateBadge().catch((e) => {
        res.send({message: {success: false, error: e.message}})
    })
    res.send({
        message
    })
}

export default handler
