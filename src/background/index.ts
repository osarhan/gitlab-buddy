import dayjs from 'dayjs'
import ky, { type KyInstance } from 'ky'
import { HIDE_OPTIONS, VIEW_OPTIONS } from '~lib/enums'
import { readStorage, setStorage } from '~lib/useStorage'
import { getUrlSchemas } from '~lib/utils'
import type { Discussions, Group, MergeRequestData, MergeRequests, Project, UserData } from '~lib/types'
import isSameOrBefore from 'dayjs/plugin/isSameOrBefore'
import { sendEvent } from '~lib/analytics'

dayjs.extend(isSameOrBefore)

chrome.runtime.onInstalled.addListener(function callBack() {
  console.log('initialized app')
  void sendEvent('app-install', {})
})

if (process.env.PLASMO_BROWSER === 'chrome')
  chrome.sidePanel.setPanelBehavior({ openPanelOnActionClick: true }).catch((error) => console.error(error))

if (process.env.PLASMO_BROWSER === 'firefox')
  browser.action.onClicked.addListener(() => {
    void browser.sidebarAction.toggle()
  })

async function checkAlarmPermissions() {
  try {
    const hasPermissions = await chrome.permissions.contains({ permissions: ['alarms'] })
    if (hasPermissions) {
      chrome.alarms.onAlarm.addListener(async (alarm) => {
        if (alarm.name === 'refresh') {
          try {
            console.log('Background refresh...')
            const resp = await getMergeRequests()
            try {
              await chrome.runtime.sendMessage(resp)
            } catch {
              await setStorage('mergeRequests', resp.mergeRequests)
              await setStorage('refreshDateStamp', Date.now())
            }
          } catch (error) {
            console.error('Failed to fetch merge requests: ', error)
          }
        }
      })
    }
  } catch (error) {
    console.error('Error checking permissions:', error)
  }
}

void checkAlarmPermissions()

export async function updateAlarm() {
  const refresh = await readStorage<string>('bgRefreshInterval')
  const refreshInt = parseInt(refresh)
  if (!refreshInt) {
    void chrome.alarms.clearAll()
  } else {
    const alarmPermission = await chrome.permissions.contains({ permissions: ['alarms'] })
    if (alarmPermission) {
      void chrome.alarms.clearAll()
      void chrome.alarms.create('refresh', { periodInMinutes: refreshInt })
    }
  }
}

export async function updateBadge(count = null) {
  const mergeRequests = await readStorage<MergeRequests>('mergeRequests')
  count = count || mergeRequests.not_approved.length
  const icon = await readStorage('icon')
  const iconColor = await readStorage<string>('iconColor')
  if (icon && count > 0) {
    void chrome.action.setBadgeBackgroundColor({ color: iconColor || '#F00' })
    void chrome.action.setBadgeText({ text: count.toString() })
  } else {
    void chrome.action.setBadgeText({ text: '' })
  }
}

function hostUrlScheme(url: string) {
  let urlScheme = url
  if (!url.startsWith('https://') || !!url.startsWith('http://')) urlScheme = `*://*.${url}`
  if (!url.endsWith('/')) urlScheme = `${urlScheme}/`
  return urlScheme
}

async function checkPermissions(url: string) {
  let urlScheme = hostUrlScheme(url)
  return chrome.permissions
    .contains({
      permissions: ['cookies'],
      origins: [urlScheme]
    })
    .then((res) => res)
}

async function requestPermissions(url: string) {
  let urlScheme = hostUrlScheme(url)
  chrome.permissions.request(
    {
      permissions: ['cookies'],
      origins: [urlScheme]
    },
    (granted) => {
      if (!granted) {
        throw new Error('No Permissions Granted')
      }
    }
  )
}

async function getSessionToken(url: string) {
  return chrome.cookies.get({ url: url, name: '_gitlab_session' }).then((cookie) => {
    return cookie
  })
}

async function getCookies(GITLAB_BASE_URL_HTTP: string, GITLAB_BASE_URL: string): Promise<string> {
  const hasPermissions = await checkPermissions(GITLAB_BASE_URL)
  if (!hasPermissions) {
    await requestPermissions(GITLAB_BASE_URL)
  }

  const cookie = await getSessionToken(GITLAB_BASE_URL_HTTP)
  if (!cookie) {
    throw new Error('No Cookie Found, Check Url or Permissions')
  }
  return `_gitlab_session=${cookie['value']};`
}

// GitLab API client setup
async function getGitlabKyClient(): Promise<KyInstance> {
  const token = await readStorage<string>('userToken')
  const gitlabUrl = await readStorage<string>('gitlabUrl')
  const urlSchema = getUrlSchemas(gitlabUrl)
  const headers: Record<string, string> = {
    Accept: 'application/json'
  }
  if (token) {
    headers['Authorization'] = `Bearer ${token}`
  } else {
    headers['Set-Cookie'] = await getCookies(urlSchema.GITLAB_BASE_URL_HTTP, urlSchema.GITLAB_BASE_URL)
  }
  return ky.create({
    prefixUrl: urlSchema.GITLAB_BASE_URL_API,
    headers,
    timeout: 10000
  })
}

async function getGitlab(path: string) {
  const GitlabKyClient = await getGitlabKyClient()
  return await GitlabKyClient.get(path).json()
}

export async function search(msg: string | number) {
  let projects = []
  let groups = []
  let results = {
    projects,
    groups
  }

  try {
    if (typeof msg === 'string' && !isNaN(Number(msg))) {
      msg = Number(msg)
    }

    if (typeof msg === 'number') {
      try {
        try {
          groups = [await getGitlab(`groups/${msg}`)]
        } catch {}
        try {
          projects = [await getGitlab(`projects/${msg}`)]
        } catch {}
        results = { projects, groups }
      } catch {
        return {
          success: true,
          results
        }
      }
    } else if (typeof msg === 'string') {
      try {
        //@ts-ignore
        projects = await getGitlab(`search?per_page=5&scope=projects&search=${encodeURIComponent(msg)}`)
      } catch {}
      try {
        //@ts-ignore
        groups = await getGitlab(`groups?per_page=5&search=${encodeURIComponent(msg)}`)
      } catch {}
      // @ts-ignore
      results = { projects, groups }
    } else {
      return {
        success: true,
        results
      }
    }
    return {
      success: true,
      results
    }
  } catch (error) {
    return {
      success: false,
      error: error instanceof Error ? error.message : 'An unknown error occurred'
    }
  }
}

export async function login() {
  let userData = await getGitlab('user')
  await setStorage('userData', userData)
  return { success: true }
}

async function getMergeRequests(): Promise<{
  success: boolean
  mergeRequests: MergeRequests
  newHiddenMRs: Array<object>
  time: number
}> {
  const viewOptions = await readStorage<Array<string>>('viewOptions') || ['0', '1', '2']
  const hideOptions = await readStorage<Array<string>>('hideOptions') || []
  const mergedDaysLookBack = await readStorage<string>('mergedDaysLookBack') || '2'
  const existingMRs: Record<string, MergeRequestData> = (await readStorage('existingMRs')) || {}
  let userData = await readStorage<UserData>('userData')

  const start = performance.now()

  const GitlabKyClient = await getGitlabKyClient()

  userData = userData || (await GitlabKyClient.get('user').json<UserData>())

  await setStorage('userData', userData)

  let mrs = await fetchMergeRequests(GitlabKyClient, userData.id, viewOptions, Number(mergedDaysLookBack))

  const excludedProjects = await readStorage<Array<Project>>('excludedProjects') || []
  const excludedGroups = await readStorage<Array<Group>>('excludedGroups') || []

  mrs = mrs.filter((mr) => {
    return (
      !excludedProjects.some((p: Project) => p.id === mr.project_id) &&
      !excludedGroups.some((g: Group) => mr.references.full.includes(g.full_path))
    )
  })
  let mrsDict = mrs.reduce(
    (acc, mr) => {
      const key = `${mr.project_id}-${mr.iid}`
      acc[key] = mr
      return acc
    },
    {} as Record<string, MergeRequestData>
  )

  mrsDict = await fetchAdditionalData(GitlabKyClient, mrsDict, mrs, existingMRs)
  const { newHiddenMRs, filteredMrs } = processHiddenMRs(
    Object.values(mrsDict),
    await readStorage<[{ id: number; date: number }]>('hiddenMRs')
  )

  const { approved, notApproved, created, merged } = categorizeMergeRequests(
    filteredMrs,
    userData.id,
    hideOptions
  )
  let time_ms = performance.now() - start
  void updateBadge(notApproved.length)
  void sendEvent('performance', { time_ms: time_ms })

  return {
    success: true,
    mergeRequests: { approved, not_approved: notApproved, created, merged, userData },
    newHiddenMRs,
    time: time_ms
  }
}

async function fetchMergeRequests(
  client: KyInstance,
  userId: number,
  viewOptions: string[],
  mergedDaysLookBack: number
): Promise<MergeRequestData[]> {
  const currentDate = new Date()
  currentDate.setDate(currentDate.getDate() - mergedDaysLookBack)
  const isoDate = currentDate.toISOString()

  const commonParams = '&per_page=20&scope=all&non_archived=true&with_labels_details=true'
  const openedSuffix = `${commonParams}&state=opened`
  const mergedSuffix = `${commonParams}&state=merged&updated_after=${isoDate}`

  const urls = [`merge_requests?author_id=${userId}${openedSuffix}`]
  if (mergedDaysLookBack > 0) {
    urls.push(`merge_requests?author_id=${userId}${mergedSuffix}`)
  }

  if (viewOptions.includes(VIEW_OPTIONS.assigneee)) {
    urls.push(`merge_requests?assignee_id=${userId}${openedSuffix}`)
    if (mergedDaysLookBack > 0) {
      urls.push(`merge_requests?assignee_id=${userId}${mergedSuffix}`)
    }
  }
  if (viewOptions.includes(VIEW_OPTIONS.reviewer)) {
    urls.push(`merge_requests?reviewer_id=${userId}${openedSuffix}`)
    if (mergedDaysLookBack > 0) {
      urls.push(`merge_requests?reviewer_id=${userId}${mergedSuffix}`)
    }
  }
  if (viewOptions.includes(VIEW_OPTIONS.group_approvals)) {
    urls.push(`merge_requests?approver_ids[]=${userId}${openedSuffix}`)
    if (mergedDaysLookBack > 0) {
      urls.push(`merge_requests?approver_ids[]=${userId}${mergedSuffix}`)
    }
  }

  const subscribedProjects: Project[] = (await readStorage('subscribedProjects')) || []
  subscribedProjects.forEach((project: Project) => {
    urls.push(`projects/${project.id}/merge_requests?${openedSuffix}`)
    if (mergedDaysLookBack > 0) urls.push(`projects/${project.id}/merge_requests?${mergedSuffix}}`)
  })
  const subscribedGroups: Group[] = (await readStorage('subscribedGroups')) || []
  subscribedGroups.forEach((group: Group) => {
    urls.push(`groups/${group.id}/merge_requests?${openedSuffix}`)
    if (mergedDaysLookBack > 0) urls.push(`groups/${group.id}/merge_requests?${mergedSuffix}`)
  })

  // Fetch all pages for each URL
  const allResponses: MergeRequestData[][] = await Promise.all(
    urls.map(async (url: string) => {
      let allData: MergeRequestData[] = []
      let page = 1
      let totalPages = 1

      do {
        const paginatedUrl = `${url}&page=${page}`
        const response = await client.get(paginatedUrl)
        totalPages = Math.min(parseInt(response.headers.get('X-Total-Pages') || '1', 10), 5)

        const data: MergeRequestData[] = await response.json()
        allData = allData.concat(data)
        page++
      } while (page <= totalPages)

      return allData
    })
  )

  // Flatten and map the responses
  return allResponses.flatMap((mrs, index) => {
    const prefix = urls[index].startsWith('merge') ? urls[index].slice(15, 17) : 'er'
    const mrType = { au: 'Author', as: 'Assignee', re: 'Reviewer', ap: 'Group Approvals', er: 'Subscriber' }[
      prefix
    ]

    return mrs.map((mr) => ({
      ...mr,
      mrType,
      project_name: mr.references.full.split('!')[0].split('/').pop(),
      project_url: `${new URL(mr.web_url).origin}/${mr.references.full.split('!')[0]}`
    }))
  })
}

async function fetchAdditionalData(
  client: KyInstance,
  mrsD: Record<string, any>,
  mrs: Array<MergeRequestData>,
  existingMRs: Record<string, any>
) {
  const mrsDict = mrsD
  const urls: string[] = []

  // Generate URLs to fetch
  mrs.forEach((mr: MergeRequestData) => {
    const cache_key = `${mr.project_id}-${mr.iid}`
    const baseUrl = `projects/${mr.project_id}/merge_requests/${mr.iid}`
    if (cache_key in existingMRs && dayjs(mr.updated_at).isSameOrBefore(existingMRs[cache_key].updated_at)) {
      mrsDict[cache_key] = existingMRs[cache_key]
      if (mr.state === 'opened') {
        urls.push(`${baseUrl}/approvals`)
      }
    } else if (mr.state === 'opened') {
      urls.push(
        baseUrl,
        `${baseUrl}/approvals`,
        `${baseUrl}/discussions?per_page=100`,
        `${baseUrl}/closes_issues`
      )
    }
  })

  // Function to process URLs in chunks
  const processInChunks = async (urls: string[], chunkSize: number) => {
    for (let i = 0; i < urls.length; i += chunkSize) {
      const chunk = urls.slice(i, i + chunkSize)
      await Promise.all(
        chunk.map((url: string) =>
          client
            .get(url)
            .json()
            .then((resp: MergeRequestData | Discussions[]) => {
              const splitUrl: string[] = url.split('/')
              const mrKey = `${splitUrl[1]}-${splitUrl[3]}`
              if (splitUrl.length === 4) {
                mrsDict[mrKey].pipeline_data = resp['head_pipeline']
              } else if (splitUrl[4] === 'approvals') {
                // approvals
                mrsDict[mrKey].approved_by = resp['approved_by']
              } else if (splitUrl[4] === 'closes_issues') {
                mrsDict[mrKey].issues = resp
              } else {
                // discussion
                mrsDict[mrKey].discussions = resp
              }
            })
            .catch((error) => {
              console.error(`Failed to fetch ${url}:`, error)
            })
        )
      )
    }
  }

  // Process URLs in chunks of 50 (adjust chunkSize as needed)
  const chunkSize = 50
  await processInChunks(urls, chunkSize)

  // Save updated data to storage
  await setStorage('existingMRs', mrsDict)
  return mrsDict
}

async function getCommentsAwards(mergeRequests: MergeRequestData[], client: KyInstance) {
  const urls = mergeRequests.flatMap((mr) =>
    mr.state === 'opened' && mr.discussions.length > 0
      ? mr.discussions[0].notes
          .filter((n) => (!n.system && !n.resolvable) || (n.resolvable && !n.resolved))
          .map((n) => `projects/${mr.project_id}/merge_requests/${mr.iid}/notes/${n.id}/award_emoji`)
      : []
  )

  const awardsResponses = await Promise.all(urls.map((url) => client.get(url).json()))

  const awards = awardsResponses.flat()

  const awardMap = awards.reduce((acc: any, award: any) => {
    if (!acc[award.awardable_id]) {
      acc[award.awardable_id] = []
    }
    acc[award.awardable_id].push(award)
    return acc
  }, {})

  mergeRequests.forEach((mr) => {
    if (mr.state === 'opened' && mr.discussions.length > 0) {
      mr.discussions[0].notes.forEach((n) => {
        if ((!n.system && !n.resolvable) || (n.resolvable && !n.resolved)) {
          n.awards = awardMap[n.id] || []
        }
      })
    }
  })

  return mergeRequests
}

function processHiddenMRs(
  mrs: MergeRequestData[],
  hiddenMRs: [{ id: number; date: number }]
): { newHiddenMRs: Array<object>; filteredMrs: MergeRequestData[] } {
  const newHiddenMRs =
    hiddenMRs?.filter((hiddenMR: { id: number; date: number }) => mrs.some((mr) => mr.id === hiddenMR.id)) ||
    []

  const filteredMrs = mrs.filter((mr) => {
    const hiddenMR = newHiddenMRs.find((hMR) => hMR.id === mr.id)
    if (hiddenMR && dayjs(hiddenMR.date).isBefore(mr.updated_at)) {
      newHiddenMRs.splice(newHiddenMRs.indexOf(hiddenMR), 1)
    }
    return !hiddenMR || dayjs(hiddenMR.date).isBefore(mr.updated_at)
  })

  return { newHiddenMRs, filteredMrs }
}

function categorizeMergeRequests(
  mrs: MergeRequestData[],
  userId: number,
  hideOptions: string[]
): {
  approved: MergeRequestData[]
  notApproved: MergeRequestData[]
  created: MergeRequestData[]
  merged: MergeRequestData[]
} {
  const approved: MergeRequestData[] = []
  const notApproved: MergeRequestData[] = []
  const created: MergeRequestData[] = []
  const merged: MergeRequestData[] = []

  mrs.forEach((mr) => {
    if (mr.state === 'closed' || mr.state === 'locked') return
    if (mr.state === 'merged') return merged.push(mr)
    if (!shouldShow(mr, hideOptions)) return
    if (mr.author.id === userId) return created.push(mr)
    if (mr.approved_by?.some((approver) => approver.user.id === userId)) return approved.push(mr)
    notApproved.push(mr)
  })
  merged.sort((a, b) => new Date(b.merged_at).getTime() - new Date(a.merged_at).getTime())

  return { approved, notApproved, created, merged }
}

function shouldShow(mr: MergeRequestData, hideOptions: string[]): boolean {
  if (!hideOptions) return true
  if (hideOptions.includes(HIDE_OPTIONS.draft) && mr.draft) return false
  if (hideOptions.includes(HIDE_OPTIONS.merge_conflicts) && mr.has_conflicts) return false
  return !(hideOptions.includes(HIDE_OPTIONS.broken_pipeline) && mr.pipeline_data?.status === 'failed')
}

export default getMergeRequests
