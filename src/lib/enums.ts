export const ACTIONS = {
    AUTH: 'auth',
    MERGE_REQUESTS: 'mergeRequests',
    BADGE: 'badge',
    ICON_TOGGLE: 'iconToggle',
    LOGIN: 'login',
    UPDATE_REFRESH: 'updateRefresh',
    SET_ALARM: 'setAlarmListener',
    SEARCH: 'search',
    DISCUSSIONS: 'discussions',
}

export const VIEW_OPTIONS = {
    assigneee: '0',
    reviewer: '1',
    group_approvals: '2'
}
export const VIEW_DATA = [
    {id: '0', name: 'Assignee'},
    {id: '1', name: 'Reviewer'},
    {id: '2', name: 'Group Approval'}
]
export const HIDE_DATA = [
    {id: '0', name: 'Draft'},
    {id: '1', name: 'Broken Pipeline'},
    {id: '2', name: 'Merge Conflicts'}
]
export const HIDE_OPTIONS = {
    draft: '0',
    broken_pipeline: '1',
    merge_conflicts: '2'
}
export const INIT_STATE = {
    approved: [],
    not_approved: [],
    created: [],
    merged: [],
    userData: {}
}
export const VIEWS = {
    NOT_APPROVED: 'not_approved',
    CREATED: 'created',
    APPROVED: 'approved',
    MERGED: 'merged'
}
export const CATEGORIES = {
    approved: {
        name: VIEWS.APPROVED,
        label: 'Approved',
        headerText: 'Approved merge requests',
        emptyListText: "You haven't approved anything recently."
    },
    created: {
        name: VIEWS.CREATED,
        label: 'Created',
        headerText: 'Created merge requests',
        emptyListText: "You don't have any open merge requests."
    },
    not_approved: {
        name: VIEWS.NOT_APPROVED,
        label: 'Pending',
        headerText: 'Pending merge requests',
        emptyListText: "You don't have any pending merge requests."
    },
    merged: {
        name: VIEWS.MERGED,
        label: 'Merged',
        headerText: 'Recently Merged MRs',
        emptyListText: "You don't have any recently merged merge requests."
    }
}
