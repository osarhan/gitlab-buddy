import {sendEvent} from "~lib/analytics";

export const openNewTab = (e, link, page) => {

    void chrome.tabs.create({
        url: link,
        active: !(e.ctrlKey || e.metaKey)
    })
    void sendEvent('link_opened', {
        page: page
    })
}

export function getUrlSchemas(url) {
    const GITLAB_BASE_URL = url
    let GITLAB_BASE_URL_HTTP = GITLAB_BASE_URL.startsWith('https://')
        ? GITLAB_BASE_URL
        : `https://${GITLAB_BASE_URL}`
    const GITLAB_BASE_URL_API = GITLAB_BASE_URL_HTTP.endsWith('/')
        ? `${GITLAB_BASE_URL_HTTP}api/v4`
        : `${GITLAB_BASE_URL_HTTP}/api/v4`
    let GITLAB_COOKIE_SCHEME = url
    if (!url.startsWith('https://') || !!url.startsWith('http://')) GITLAB_COOKIE_SCHEME = `*://*.${url}`
    if (!url.endsWith('/')) GITLAB_COOKIE_SCHEME = `${GITLAB_COOKIE_SCHEME}/`

    return {
        GITLAB_BASE_URL,
        GITLAB_BASE_URL_HTTP,
        GITLAB_BASE_URL_API,
        GITLAB_COOKIE_SCHEME
    }
}


export const debounce = (func, delay) => {
    let timer;
    return (...args) => {
        clearTimeout(timer);
        timer = setTimeout(() => func(...args), delay);
    };
};