import ky from 'ky'

const GA = 'https://www.google-analytics.com/mp/collect'
const SESSION_EXPIRATION_IN_MIN = 30
addEventListener('unhandledrejection', async (event) => {
  await sendEvent('extension_error', {
    message: event.reason.message,
    stack: event.reason.stack
  })
})

export async function sendEvent(name: string, params: Record<string, string | number>) {
  const DEFAULT_ENGAGEMENT_TIME_IN_MSEC = 100
  ky.post(
    `${GA}?measurement_id=${process.env.PLASMO_PUBLIC_GTAG_ID}&api_secret=${process.env.PLASMO_PUBLIC_GA_API}`,
    {
      method: 'POST',
      body: JSON.stringify({
        client_id: await getOrCreateClientId(),
        events: [
          {
            name,
            params: {
              ...params,
              session_id: await getOrCreateSessionId(),
              engagement_time_msec: DEFAULT_ENGAGEMENT_TIME_IN_MSEC,
              version:
                process.env.PLASMO_BROWSER === 'firefox'
                  ? browser.runtime.getManifest().version
                  : chrome.runtime.getManifest().version
            }
          }
        ]
      })
    }
  )
}

async function getOrCreateSessionId() {
  let { sessionData } = await chrome.storage.session.get('sessionData')
  const currentTimeInMs = Date.now()

  if (sessionData && sessionData.timestamp) {
    const durationInMin = (currentTimeInMs - sessionData.timestamp) / 60000

    if (durationInMin > SESSION_EXPIRATION_IN_MIN) {
      sessionData = null
    } else {
      sessionData.timestamp = currentTimeInMs
      await chrome.storage.session.set({ sessionData })
    }
  }

  if (!sessionData) {
    sessionData = {
      session_id: currentTimeInMs.toString(),
      timestamp: currentTimeInMs.toString()
    }
    await chrome.storage.session.set({ sessionData })
  }
  return sessionData.session_id
}

async function getOrCreateClientId() {
  const result = await chrome.storage.local.get('clientId')
  let clientId = result.clientId
  if (!clientId) {
    // Generate a unique client ID, the actual value is not relevant
    clientId = self.crypto.randomUUID()
    await chrome.storage.local.set({ clientId })
  }
  return clientId
}
