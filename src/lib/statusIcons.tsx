import React from 'react'

export const Canceled = ({h, w, color}) => (
    <svg
        width={w}
        height={h}
        viewBox="0 0 14 14"
        className="bg-transparent"
        xmlns="http://www.w3.org/2000/svg">
        <g fillRule="evenodd">
            <path d="M0 7a7 7 0 1 1 14 0A7 7 0 0 1 0 7z"/>
            <path
                d="M13 7A6 6 0 1 0 1 7a6 6 0 0 0 12 0z"
                fill="#FFF"
                style={{fill: `var(--svg-status-bg, ${color})`}}
            />
            <path
                d="M5.2 3.8l4.9 4.9c.2.2.2.5 0 .7l-.7.7c-.2.2-.5.2-.7 0L3.8 5.2c-.2-.2-.2-.5 0-.7l.7-.7c.2-.2.5-.2.7 0"/>
        </g>
    </svg>
)

export const Success = ({h, w, color}) => (
    <svg
        width={w}
        height={h}
        viewBox="0 0 14 14"
        className="bg-transparent"
        xmlns="http://www.w3.org/2000/svg">
        <g>
            <path d="M0 7a7 7 0 1 1 14 0A7 7 0 0 1 0 7z"/>
            <path
                d="M13 7A6 6 0 1 0 1 7a6 6 0 0 0 12 0z"
                style={{fill: `var(--svg-status-bg, ${color})`}}
            />
            <path
                d="M6.278 7.697L5.045 6.464a.296.296 0 0 0-.42-.002l-.613.614a.298.298 0 0 0 .002.42l1.91 1.909a.5.5 0 0 0 .703.005l.265-.265L9.997 6.04a.291.291 0 0 0-.009-.408l-.614-.614a.29.29 0 0 0-.408-.009L6.278 7.697z"/>
        </g>
    </svg>
)

export const Closed = ({h, w, color}) => (
    <svg
        width={w}
        height={h}
        viewBox="0 0 16 16"
        xmlns="http://www.w3.org/2000/svg">
        <path
            d="M7.536 8.657l2.828-2.83c.39-.39 1.024-.39 1.414 0 .39.392.39 1.025 0 1.416l-3.535 3.535c-.196.195-.452.293-.707.293-.256 0-.512-.097-.708-.292l-2.12-2.12c-.39-.392-.39-1.025 0-1.415s1.023-.39 1.413 0zM8 16c-4.418 0-8-3.582-8-8s3.582-8 8-8 8 3.582 8 8-3.582 8-8 8zm0-2c3.314 0 6-2.686 6-6s-2.686-6-6-6-6 2.686-6 6 2.686 6 6 6z"/>
    </svg>
)
export const Created = ({h, w, color}) => (
    <svg
        width={w}
        height={h}
        viewBox="0 0 14 14"
        xmlns="http://www.w3.org/2000/svg">
        <g fillRule="evenodd">
            <path d="M0 7a7 7 0 1 1 14 0A7 7 0 0 1 0 7z"/>
            <path
                d="M13 7A6 6 0 1 0 1 7a6 6 0 0 0 12 0z"
                fill="#FFF"
                style={{fill: `var(--svg-status-bg, ${color})`}}
            />
            <circle cx="7" cy="7" r="3.25"/>
        </g>
    </svg>
)
export const Failed = ({h, w, color}) => (
    <svg
        width={w}
        height={h}
        viewBox="0 0 14 14"
        xmlns="http://www.w3.org/2000/svg">
        <g fillRule="evenodd">
            <path d="M0 7a7 7 0 1 1 14 0A7 7 0 0 1 0 7z"/>
            <path
                d="M13 7A6 6 0 1 0 1 7a6 6 0 0 0 12 0z"
                fill="#FFF"
                style={{fill: `var(--svg-status-bg, ${color})`}}
            />
            <path
                d="M7 5.969L5.599 4.568a.29.29 0 0 0-.413.004l-.614.614a.294.294 0 0 0-.004.413L5.968 7l-1.4 1.401a.29.29 0 0 0 .004.413l.614.614c.113.114.3.117.413.004L7 8.032l1.401 1.4a.29.29 0 0 0 .413-.004l.614-.614a.294.294 0 0 0 .004-.413L8.032 7l1.4-1.401a.29.29 0 0 0-.004-.413l-.614-.614a.294.294 0 0 0-.413-.004L7 5.968z"/>
        </g>
    </svg>
)
export const Manual = ({h, w, color}) => (
    <svg
        width={w}
        height={h}
        viewBox="0 0 14 14"
        xmlns="http://www.w3.org/2000/svg">
        <g fillRule="evenodd">
            <path d="M0 7a7 7 0 1 1 14 0A7 7 0 0 1 0 7z"/>
            <path
                d="M13 7A6 6 0 1 0 1 7a6 6 0 0 0 12 0z"
                fill="#FFF"
                style={{fill: `var(--svg-status-bg, ${color})`}}
            />
            <path
                d="M10.5 7.63V6.37l-.787-.13c-.044-.175-.132-.349-.263-.61l.481-.652-.918-.913-.657.478a2.346 2.346 0 0 0-.612-.26L7.656 3.5H6.388l-.132.783c-.219.043-.394.13-.612.26l-.657-.478-.918.913.437.652c-.131.218-.175.392-.262.61l-.744.086v1.261l.787.13c.044.218.132.392.263.61l-.438.651.92.913.655-.434c.175.086.394.173.613.26l.131.783h1.313l.131-.783c.219-.043.394-.13.613-.26l.656.478.918-.913-.48-.652c.13-.218.218-.435.262-.61l.656-.13zM7 8.283a1.285 1.285 0 0 1-1.313-1.305c0-.739.57-1.304 1.313-1.304.744 0 1.313.565 1.313 1.304 0 .74-.57 1.305-1.313 1.305z"/>
        </g>
    </svg>
)
export const NotFound = ({h, w, color}) => (
    <svg
        width={w}
        height={h}
        viewBox="0 0 14 14"
        xmlns="http://www.w3.org/2000/svg">
        <path d="M0 7c0-3.866 3.142-7 7-7 3.866 0 7 3.142 7 7 0 3.866-3.142 7-7 7-3.866 0-7-3.142-7-7z"/>
        <path
            d="M1 7c0 3.309 2.69 6 6 6 3.309 0 6-2.69 6-6 0-3.309-2.69-6-6-6-3.309 0-6 2.69-6 6z"
            fill="#FFF"
            style={{fill: `var(--svg-status-bg, ${color})`}}
        />
        <path
            d="M7 9.219a2.218 2.218 0 1 0 0-4.436A2.218 2.218 0 0 0 7 9.22zm0 1.12a3.338 3.338 0 1 1 0-6.676 3.338 3.338 0 0 1 0 6.676z"/>
    </svg>
)
export const Open = ({h, w, color}) => (
    <svg
        width={w}
        height={h}
        viewBox="0 0 14 14"
        xmlns="http://www.w3.org/2000/svg">
        <path d="M0 7c0-3.866 3.142-7 7-7 3.866 0 7 3.142 7 7 0 3.866-3.142 7-7 7-3.866 0-7-3.142-7-7z"/>
        <path
            d="M1 7c0 3.309 2.69 6 6 6 3.309 0 6-2.69 6-6 0-3.309-2.69-6-6-6-3.309 0-6 2.69-6 6z"
            fill="#FFF"
            style={{fill: `var(--svg-status-bg, ${color})`}}
        />
        <path
            d="M7 9.219a2.218 2.218 0 1 0 0-4.436A2.218 2.218 0 0 0 7 9.22zm0 1.12a3.338 3.338 0 1 1 0-6.676 3.338 3.338 0 0 1 0 6.676z"/>
    </svg>
)
export const Pending = ({h, w, color}) => (
    <svg
        width={w}
        height={h}
        viewBox="0 0 14 14"
        xmlns="http://www.w3.org/2000/svg">
        <g fillRule="evenodd">
            <path d="M0 7a7 7 0 1 1 14 0A7 7 0 0 1 0 7z"/>
            <path
                d="M13 7A6 6 0 1 0 1 7a6 6 0 0 0 12 0z"
                fill="#FFF"
                style={{fill: `var(--svg-status-bg, ${color})`}}
            />
            <path
                d="M4.7 5.3c0-.2.1-.3.3-.3h.9c.2 0 .3.1.3.3v3.4c0 .2-.1.3-.3.3H5c-.2 0-.3-.1-.3-.3V5.3m3 0c0-.2.1-.3.3-.3h.9c.2 0 .3.1.3.3v3.4c0 .2-.1.3-.3.3H8c-.2 0-.3-.1-.3-.3V5.3"/>
        </g>
    </svg>
)
export const Preparing = ({h, w, color}) => (
    <svg
        xmlns="http://www.w3.org/2000/svg"
        width={w}
        height={h}
        viewBox="0 0 14 14">
        <g fillRule="evenodd">
            <g fillRule="nonzero">
                <path d="M0 7a7 7 0 1114 0A7 7 0 010 7z"/>
                <path
                    fill="#FFF"
                    d="M13 7A6 6 0 101 7a6 6 0 0012 0z"
                    style={{fill: `var(--svg-status-bg, ${color})`}}
                />
            </g>
            <circle cx="7" cy="7" r="1"/>
            <circle cx="10" cy="7" r="1"/>
            <circle cx="4" cy="7" r="1"/>
        </g>
    </svg>
)
export const Running = ({h, w, color}) => (
    <svg
        width={w}
        height={h}
        viewBox="0 0 14 14"
        xmlns="http://www.w3.org/2000/svg">
        <g fillRule="evenodd">
            <path d="M0 7a7 7 0 1 1 14 0A7 7 0 0 1 0 7z"/>
            <path
                d="M13 7A6 6 0 1 0 1 7a6 6 0 0 0 12 0z"
                fill="#FFF"
                style={{fill: `var(--svg-status-bg, ${color})`}}
            />
            <path d="M7 3c2.2 0 4 1.8 4 4s-1.8 4-4 4c-1.3 0-2.5-.7-3.3-1.7L7 7V3"/>
        </g>
    </svg>
)
export const Scheduled = ({h, w, color}) => (
    <svg
        width={w}
        height={h}
        viewBox="0 0 14 14"
        xmlns="http://www.w3.org/2000/svg">
        <path d="M7 14A7 7 0 107 0a7 7 0 000 14z" fill="#000"/>
        <path d="M7 13A6 6 0 107 1a6 6 0 000 12z" fill="#fff"/>
        <path
            d="M6.995 10.64a3.645 3.645 0 110-7.29 3.645 3.645 0 010 7.29zm0-1.042a2.603 2.603 0 100-5.206 2.603 2.603 0 000 5.206z"
            fill="#000"
        />
        <path
            d="M7.033 4.92h-.065a.488.488 0 00-.488.488v1.627c0 .27.218.488.488.488h.065c.27 0 .488-.218.488-.488V5.408a.488.488 0 00-.488-.488z"
            fill="#000"
        />
        <path
            d="M8.075 6.48H6.968a.488.488 0 00-.488.488v.065c0 .27.218.488.488.488h1.107c.27 0 .488-.218.488-.488v-.065a.488.488 0 00-.488-.488z"
            fill="#000"
        />
    </svg>
)
export const Skipped = ({h, w, color}) => (
    <svg
        width={w}
        height={h}
        viewBox="0 0 14 14"
        xmlns="http://www.w3.org/2000/svg">
        <path d="M7 14A7 7 0 1 1 7 0a7 7 0 0 1 0 14z"/>
        <path
            d="M7 13A6 6 0 1 0 7 1a6 6 0 0 0 0 12z"
            fill="#FFF"
            fillRule="nonzero"
            style={{fill: `var(--svg-status-bg, ${color})`}}
        />
        <path
            d="M6.415 7.04L4.579 5.203a.295.295 0 0 1 .004-.416l.349-.349a.29.29 0 0 1 .416-.004l2.214 2.214a.289.289 0 0 1 .019.021l.132.133c.11.11.108.291 0 .398L5.341 9.573a.282.282 0 0 1-.398 0l-.331-.331a.285.285 0 0 1 0-.399L6.415 7.04zm2.54 0L7.119 5.203a.295.295 0 0 1 .004-.416l.349-.349a.29.29 0 0 1 .416-.004l2.214 2.214a.289.289 0 0 1 .019.021l.132.133c.11.11.108.291 0 .398L7.881 9.573a.282.282 0 0 1-.398 0l-.331-.331a.285.285 0 0 1 0-.399L8.955 7.04z"/>
    </svg>
)
export const Warning = ({h, w, color}) => (
    <svg
        width={w}
        height={h}
        viewBox="0 0 14 14"
        xmlns="http://www.w3.org/2000/svg">
        <g fillRule="evenodd">
            <path d="M0 7a7 7 0 1 1 14 0A7 7 0 0 1 0 7z"/>
            <path
                d="M13 7A6 6 0 1 0 1 7a6 6 0 0 0 12 0z"
                fill="#FFF"
                style={{fill: `var(--svg-status-bg, ${color})`}}
            />
            <path
                d="M6 3.5c0-.3.2-.5.5-.5h1c.3 0 .5.2.5.5v4c0 .3-.2.5-.5.5h-1c-.3 0-.5-.2-.5-.5v-4m0 6c0-.3.2-.5.5-.5h1c.3 0 .5.2.5.5v1c0 .3-.2.5-.5.5h-1c-.3 0-.5-.2-.5-.5v-1"/>
        </g>
    </svg>
)
