export type UserData = {
    id: number
    username: string
    email: string
    name: string
    state: string
    avatar_url: string
    web_url: string
}
export type Notes = {
    id: number
    type: string
    body: string
    attachment: Object
    resolved: boolean
    resolvable: boolean
    system: boolean
    awards: Awards[]
}
export type Discussions = {
    id: number
    individual_note: boolean
    notes: Notes[]
}
export type Awards = {
    id: number
    name: string
    awardable_id: number
}
export type user = {
    id: number
    username: string
    name: string
    state: string
    avatar_url: string
    web_url: string
}
export type ApproverData = {
    approved_by: Approver[]
}
export type Approver = {
    user: user
}
export type PipelineData = {
    head_pipeline: HeadPipeline
}
export type HeadPipeline = {
    id: number
    iid: number
    project_id: number
    sha: string
    ref: string
    status: string
    source: string
    created_at: Date
    updated_at: Date
    web_url: string
    before_sha: string
    tag: boolean
    user: user
    started_at: Date
    finished_at: Date
    committed_at: Date
    duration: number
    queued_duration: number
    coverage: string
    detailed_status: {
        icon: string
        text: string
        label: string
        group: string
        tooltip: string
        has_details: boolean
        details_path: string
        favicon: string
    }
}

export type Label = {
    id: string
    name: string
    color: string
    text_color: string
}
export type MergeRequestData = {
    mrType: string
    author: UserData
    head_pipeline: HeadPipeline
    references: {
        short: string
        relative: string
        full: string
    }
    created_at: string
    description: string
    id: number
    labels: Label[]
    merge_status: string
    draft: boolean
    iid: number
    project_id: number
    state: string
    title: string
    updated_at: string
    web_url: string
    has_conflicts: boolean
    approved_by: Approver[]
    discussions: Discussions[]
    target_branch: string
    pipeline_data: HeadPipeline
    merged_at: Date
    project_name: string
    project_url: string
    user_notes_count: number
}

export type MergeRequests = {
    approved: Array<MergeRequestData>
    not_approved: Array<MergeRequestData>
    created: Array<MergeRequestData>
    merged: Array<MergeRequestData>
    userData: UserData
}

export type Project = {
    id: number
    name: string
    avatar_url: string
    name_with_namespace: string
    web_url: string

}
export type Group = {
    id: number
    name: string
    description: string
    path: string
    full_path: string
}