const { heroui } = require('@heroui/react')

/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './src/**/*.{html,js,ts,tsx}', // Include only files in the `src` directory
    './node_modules/@heroui/theme/dist/**/*.{js,ts,jsx,tsx}' // Include HeroUI theme files
  ],
  theme: {
    extend: {}
  },
  darkMode: 'class',
  plugins: [heroui()]
}
